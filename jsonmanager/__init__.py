from .jsonmanager import flatjson, JsonObject

__all__ = ["JsonObject", "flatjson"]

__version__ = "0.1.0"
